//
//  Date+Extension.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 2/9/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import Foundation

extension Date {
	func yearsFrom() -> Int{
		return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
	}
	func monthsFrom() -> Int{
		return Calendar.current.dateComponents([.month], from: self, to: Date()).month!
	}
	func weeksFrom() -> Int{
		return Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear!
	}
	func daysFrom() -> Int{
		return Calendar.current.dateComponents([.day], from: self, to: Date()).day!
	}
	func hoursFrom() -> Int{
		return Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
	}
	func minutesFrom() -> Int{
		return Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
	}
	func secondsFrom() -> Int{
		return Calendar.current.dateComponents([.second], from: self, to: Date()).second!
	}
	func offsetFrom() -> String {
		
		if yearsFrom()   > 0 { return "\(yearsFrom()) " + (yearsFrom() == 1 ? "year" : "years")   }
		if monthsFrom()  > 0 { return "\(monthsFrom()) " + (monthsFrom() == 1 ? "month" : "months")  }
		if weeksFrom()   > 0 { return "\(weeksFrom()) " + (weeksFrom() == 1 ? "week" : "weeks")   }
		if daysFrom()    > 0 { return "\(daysFrom()) " + (daysFrom() == 1 ? "day" : "days")    }
		if hoursFrom()   > 0 { return "\(hoursFrom()) " + (hoursFrom() == 1 ? "hour" : "hours")   }
		if minutesFrom() > 0 { return "\(minutesFrom()) " + (minutesFrom() == 1 ? "minute" : "minutes") }
		if secondsFrom() > 0 { return "\(secondsFrom()) " + (secondsFrom() == 1 ? "second" : "seconds") }
		return ""
	}
}
