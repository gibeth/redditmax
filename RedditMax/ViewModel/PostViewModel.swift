//
//  PostViewModel.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 3/22/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import Foundation

protocol PostViewModelDelegate : class {
	func postViewModelStartingLoading()
	func postViewModelEndLoading()
	func postViewModelFinishWithError(message: String)
}


class PostViewModel {
	
	weak var delegate: PostViewModelDelegate?
	var dataSource: PostDataSource
	var posts : [RMPost] = []
	
	init(data: PostDataSource) {
		
		self.dataSource = data
	}
	
	func loadPost() {
		
		self.delegate?.postViewModelStartingLoading()
		
		self.dataSource.getPosts { (success, posts, message) in
			if success {
				self.posts.append(contentsOf: posts)
				self.delegate?.postViewModelEndLoading()
			} else {
				self.delegate?.postViewModelFinishWithError(message: message ?? "Error")
			}
		}
	}
	
	func reloadAction() {
		self.posts = []
		self.dataSource.reset()
		loadPost()
	}
}
