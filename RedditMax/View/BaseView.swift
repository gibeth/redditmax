//
//  BaseView.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 2/8/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class BaseView: UIView {

	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.layer.masksToBounds = false
		self.layer.shadowOpacity = 0.5
		self.layer.shadowRadius = 2.0
		self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
		self.layer.shadowColor = UIColor.darkGray.cgColor
	}
	
	@IBInspectable var cornerRadius : CGFloat = 0.0 {
		didSet {
			self.layer.cornerRadius = cornerRadius
			self.layer.masksToBounds = cornerRadius > 0
		}
	}
}
