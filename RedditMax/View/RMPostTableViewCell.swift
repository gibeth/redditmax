//
//  RMPostTableViewCell.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 2/8/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import UIKit
import SDWebImage

class RMPostTableViewCell: UITableViewCell {
	
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var commentsLbl: UILabel!
	@IBOutlet weak var dateLbl: UILabel!
	@IBOutlet weak var authorLbl: UILabel!
	@IBOutlet weak var thumbnailImageView: UIImageView!
	@IBOutlet weak var numberLbl: UILabel!
	@IBOutlet weak var subredditLbl: UILabel!
	
	func configCell(post: RMPost, number: Int) {
		titleLbl.text = post.title
		commentsLbl.text = "\(post.comments!) comments"
		authorLbl.text = "By: \(post.author!)"
		thumbnailImageView.sd_setImage(with: URL(string: post.thumbnail!)!, placeholderImage: #imageLiteral(resourceName: "redditPlaceholder"))
		numberLbl.text = "\(number + 1)"
		subredditLbl.text = "/r/\(post.subreddit!)"
		dateLbl.text = post.createDateString()
	}
}
