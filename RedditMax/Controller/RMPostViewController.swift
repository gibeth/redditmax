//
//  RMPostViewController.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 2/8/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import UIKit

class RMPostViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var spinner: UIActivityIndicatorView!
	
	var loadMorePosts = false
	var loading = false
	var viewModel: PostViewModel!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		let dataSource = PostDataSourceNetwork()
		self.viewModel = PostViewModel(data: dataSource)
		self.viewModel.delegate = self
		
		self.viewModel.loadPost()
    }
	
	func showAlert(message: String?) {
		let alert = UIAlertController(title: "ERROR", message: message, preferredStyle: .alert)
		let action = UIAlertAction(title: "OK", style: .default, handler: nil)
		alert.addAction(action)
		self.present(alert, animated: true, completion: nil)
	}
	
	@IBAction func reloadAction(_ sender: UIBarButtonItem) {
		self.viewModel.reloadAction()
	}
	
}

extension RMPostViewController: UITableViewDataSource, UITableViewDelegate {

	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.viewModel.posts.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
	
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "RMPostCell") as! RMPostTableViewCell
		cell.configCell(post: (self.viewModel.posts[indexPath.row]), number: indexPath.row)
		return cell
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height + 80) {
			self.loadMorePosts =  true
		}
		
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		if self.loadMorePosts && (self.viewModel.posts.count) < 25 && !self.loading {
			self.viewModel.loadPost()
			self.loadMorePosts = false
		}
	}
}

extension RMPostViewController: PostViewModelDelegate {

	func postViewModelStartingLoading() {
		self.spinner.startAnimating()
	}
	
	func postViewModelEndLoading() {
		self.spinner.stopAnimating()
		self.tableView.reloadData()
	}
	
	func postViewModelFinishWithError(message: String) {
		self.spinner.stopAnimating()
		self.showAlert(message: message)
	}
}





