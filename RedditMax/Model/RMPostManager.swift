//
//  RMPostManager.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 2/9/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import Foundation
import SwiftyJSON

class RMPostManager {
	
	static let sharedInstance = RMPostManager()
	var after : String?

	func getPosts(done: @escaping ((_ success: Bool, _ posts: [RMPost], _ message: String?) -> Void)) {
		Network.makeRequest(after: self.after ?? "") { (response, message) in
			
			guard let data = response?["data"] as? JSON, let children = (data.dictionary)?["children"]?.array else {
				done(false, [], message ?? "An error has occurred")
				return
			}
			
			self.after = (data.dictionary)?["after"]?.string
			let posts = (children).map{ RMPost(data: $0) }
			
			done(true, posts, "")
		}
	}
}
