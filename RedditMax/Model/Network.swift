//
//  Network.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 2/8/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire
import SwiftyJSON

class Network {
	
	fileprivate static let redditURL = "https://www.reddit.com/top.json?limit=5&after="
	
	fileprivate static let errorMsg = "An error has occurred"
	
	fileprivate class func configureRequest(_ url : String, value : String) -> URLRequest{
		
		var request = URLRequest(url: URL(string: "\(url)\(value)")!)
		request.httpMethod = "GET"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		
		return request
	}
	
	static func makeRequest(after: String, done: @escaping (_ response: Dictionary<String, Any>?, _ msg: String?) -> Void) {
		
		if isConnectedToNetwork() {
			
			let request = configureRequest(redditURL, value: after)
			
			Alamofire.request(request).validate().responseJSON { response in
				
				if response.result.isSuccess {
					
					guard let value = response.result.value, let response = (JSON(value)).dictionary
						else {
							return done(nil, errorMsg)
					}
					done(response, nil)
				} else {
					done(nil, errorMsg)
				}
			}
		} else {
			done(nil, "No Internet connection")
		}
	}
}

extension Network {
	
	static func isConnectedToNetwork() -> Bool {
		
		var zeroAddress = sockaddr_in()
		zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
		zeroAddress.sin_family = sa_family_t(AF_INET)
		let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
			$0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
				SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
			}
		}
		var flags = SCNetworkReachabilityFlags()
		if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
			return false
		}
		let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
		let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
		
		return (isReachable && !needsConnection)
	}
}
