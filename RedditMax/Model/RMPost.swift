//
//  RMPost.swift
//  RedditMax
//
//  Created by Giovanna Caolo on 2/8/17.
//  Copyright © 2017 Gibeth. All rights reserved.
//

import Foundation
import SwiftyJSON

class RMPost {
	
	var title: String!
	var comments: Int!
	var author: String!
	var createdDate: Int!
	var dateString: String?
	var thumbnail: String!
	var url: String!
	var subreddit: String!
	
	init (data: JSON) {
		
		guard let info = data.dictionary?["data"]?.dictionary else {
			return
		}
		
		self.title = info["title"]?.string ?? ""
		self.comments = info["num_comments"]?.int ?? 0
		self.author = info["author"]?.string ?? ""
		self.createdDate = info["created_utc"]?.int ?? 0
		self.thumbnail = info["thumbnail"]?.string ?? ""
		self.url = info["url"]?.string ?? ""
		self.subreddit = info["subreddit"]?.string ?? ""
	}
	
	
	func createDateString() -> String {
		return "submitted " + Date(timeIntervalSince1970: TimeInterval(createdDate)).offsetFrom() + " ago"
	}
}

